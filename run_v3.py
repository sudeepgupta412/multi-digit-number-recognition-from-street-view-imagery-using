import cv2
import numpy as np
import h5py
import os
from keras.models import Sequential
from keras.layers import Dense
from keras.models import model_from_json
from keras.models import load_model
import requests
import json
import pandas as pd
from keras.preprocessing.image import ImageDataGenerator
VID_DIR = "input_videos"

#json_file = open('model.json', 'r')
#loaded_model_json = json_file.read()
#json_file.close()
#model = model_from_json(loaded_model_json)
## load weights into new model
#model.load_weights("model.h5")
#print("Loaded model from disk")
# 
## evaluate loaded model on test data
#model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
output_dir = "output"
folder = "input_images"
vidpath = 'output/frames'  
def remove_duplicates(values):
        output = []
        seen = set()
        for value in values:
            # If value has not been encountered yet,
            # ... add it to both list and set.
            if value not in seen:
                output.append(value)
                seen.add(value)
        return output
    
def contains(A, B):
        r1x1,r1y1,w,h = A
        r1y2 = r1y1 + h
        r1x2 = r1x1 + w
        
        r2x1,r2y1,w,h = B
        r2y2 = r2y1 + h
        r2x2 = r2x1 + w
        return (r1x1 <= r2x1 and r1y1 <= r2y1 and r2x2 <= r1x2 and r2y2 <= r1y2)



def load_images_and_run(folder):
    images = []
    
    model = load_model('weights.best.custom_model_plain.hdf5')
    model.summary()
    index=0
    for filename in os.listdir(folder):
        
        img = cv2.imread(os.path.join(folder,filename))
        #cv2.imshow('image',img)
        #cv2.waitKey(0)
        if img is not None:
            index = index+1
            image_pipeline(model,img,index)
    print ('images processed')

def image_pipeline(model,image,index):
    print ('Processing images..')
    img = image
    
    #Create MSER object
    mser = cv2.MSER_create()
    
    #Your image path i-e receipt path
    #img = cv2.imread('39.png')
    #img = cv2.blur(img,(3,3))
    original_image = img.copy()
    #cv2.imshow('number3',img)
    #cv2.waitKey(0)
    #Convert to gray scale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    vis = img.copy()
    
    #detect regions in gray scale image
    regions, bboxes = mser.detectRegions(gray)
    #print ('bounding boxes =' + str(bboxes))
    hulls = [cv2.convexHull(p.reshape(-1, 1, 2)) for p in regions]
    #print ('hulls')
    #print (hulls)
    
    #print ('length of all hulls = ' + str(len(hulls)))
    #cv2.polylines(vis, hulls, 1, (0, 255, 0))
    
    #cv2.imshow('img', vis)
    
    #cv2.waitKey(0)
    
    
    
    
    #hulls = remove_duplicates(np.asarray(hulls))
    #print ('length of new hulls = ' + str(len(hulls)))
    
    mask = np.zeros((img.shape[0], img.shape[1], 1), dtype=np.uint8)
    
    
    coordinates = []
    for contour in hulls:
        #temp = img
        x,y,w,h = cv2.boundingRect(contour)
        #cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)
        #image_temp = temp[y:y+h, x:x+w]
        #cv2.imshow('image_temp', image_temp)
        #cv2.waitKey(0)
        #images.append(image_temp)
        coordinates.append((x,y,w,h))
        #cv2.drawContours(mask, [contour], -1, (255, 255, 255), -1)
    
    
    #print ('coordinates length = ' +str(len(coordinates)))
    
    coordinates = remove_duplicates(coordinates)
    coordinates = np.asarray(coordinates)
    #print ('new coordinates length = ' +str(len(coordinates)))
    
    mean_coords = np.mean(coordinates,axis=0)
    new_coords = []
    for coord in coordinates:
        if coord[3] > mean_coords[3]:
            if coord[2]/coord[3] < 1.25:
                new_coords.append(coord)
    
    #print ('lenght of final coordinates = ' + str(len(new_coords)))
    
    coordinates = new_coords
    
    #print (new_coords)
    
    
    
    new_coordinates = []
    for i in range(len(new_coords)):
        coords = new_coords[i]
        #print (coords)
        x,y,w,h=coords
        flag = 0
        for j in range(len(new_coords)):
            if (i==j):
                continue
            coords1 = new_coords[j]
            #print (coords1)
            x1,y1,w1,h1=coords1
            #print (contains(coords, coords1))
            if contains(coords1, coords) == True:
                flag = 1
        if flag ==0:
            new_coordinates.append(coords)
                #print (j)
                #new_coordinates.pop(j-remove_count)
                
                
    
    #print (new_coords)
    coordinates = new_coordinates
    
    
    
    images = []
    for coords in coordinates:
        temp = img
        x,y,w,h = coords
        image_temp = temp[y:y+h, x:x+w]
        images.append(image_temp)
    #this is used to find only text regions, remaining are ignored
    #
    path = 'OUTPUT'   
    output_image = original_image
    font = cv2.FONT_HERSHEY_SIMPLEX
    for index, img in enumerate(images):
    #   
        #cv2.imshow('image', img)
        #cv2.waitKey(0)
        img = cv2.resize(img, (32,32),interpolation=cv2.INTER_CUBIC)
    #cv2.imwrite('./data/test_32x32/num3.png',img)
        img = img[np.newaxis,...]
    #test_generator = test_datagen.flow_from_directory(
    #        'data/test_32x32',
    #        target_size=(32, 32),
    #        batch_size=128,
    #        class_mode='categorical',
    #        shuffle=False
    #        )
    #preds = model.predict_generator(test_generator, verbose=1)
    #pred_labels = np.argmax(preds,axis=1)
    #print ('predicted output =' + str(pred_labels))
        x,y,w,h = coordinates[index]
        preds = model.predict(img, verbose=0)
        pred_labels = np.argmax(preds,axis=1)
        #print ('predicted output =' + str(pred_labels))
        if pred_labels < 10:
            cv2.rectangle(output_image,(x,y),(x+w,y+h),(0,255,0),2)
            cv2.putText(output_image,str(pred_labels[0]),(x,y), font, 1,(0,255,0),2,cv2.LINE_AA)
    
    cv2.imwrite(os.path.join(path , str(index)+'.jpg'), output_image)
    #cv2.imshow('output image', output_image)   
    #cv2.waitKey(0)


def video_frame_generator(filename):
    """A generator function that returns a frame on each 'next()' call.

    Will return 'None' when there are no frames left.

    Args:
        filename (string): Filename.

    Returns:
        None.
    """
    # Todo: Open file with VideoCapture and set result to 'video'. Replace None
    
    #print 'in video frame generator'
    video = cv2.VideoCapture(filename)

    # Do not edit this while loop
    while video.isOpened():
        ret, frame = video.read()

        if ret:
            yield frame
        else:
            break

    # Todo: Close video (release) and yield a 'None' value. (add 2 lines)
    
    video.release()
    
    yield None

def video():

    
    #video_file = "my_video.mp4"  # Place your video in the input_video directory
    video_file = "my_video.mp4"
    frame_ids = [55, 60]
    fps = 10
    
    model = initialize_load_model()

    cap = cv2.VideoCapture(video_file)
    #width = cap.get(cv2.CV_CAP_PROP_FRAME_WIDTH) 
    #height = cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT)
    fourcc = cv2.VideoWriter_fourcc(*'X264')
    #out = cv2.VideoWriter('output.avi',fourcc, 10.0, (1080,1920))
    out = cv2.VideoWriter('output1.avi',-1, 20.0, (1080,1920))
    frame_num = 0
    while(cap.isOpened()):
        ret, frame = cap.read()
        if ret==True:
            frame_num = frame_num+1
            frame = cv2.flip(frame,0)
            frame = cv2.flip(frame,0)
            #print (np.shape(frame))
            print('processing video..')
            output = detect_and_classify(model,frame)
            #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            #out.write(output)
            frame_out = output.copy()
            frame_out = cv2.resize(frame_out,(800,461))
            #cv2.imshow('frame',output)
            cv2.imwrite(os.path.join(vidpath , str(frame_num)+'.jpg'), frame_out)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        else:
            break
    cap.release()
    out.release()
    cv2.destroyAllWindows()
    # Todo: Complete this part on your own.        
    

# =============================================================================
#     video= os.path.join(VID_DIR, video_file)
#     video_gen = video_frame_generator(video)
# 
#     video_image = video_gen.next()
#     
#     #print video_image
#     #cv2.imshow('frame', video_image)
#     #cv2.waitKey(0)
#     h, w, d = video_image.shape
# 
#     output_name = "ps4-6-a-2"
#     out_path = "output/ar_{}-{}".format(output_name[4:], video_file)
#     video_final = mp4_video_writer(out_path, (w, h), fps)
#     
# 
#     
#     counter_init = 1
#     output_counter = counter_init
#     output_name = "ps4-6-a"
#     frame_num = 1
#     image_counter = 1
#     model = initialize_load_model()
#     for frame_num in range(200):
#         video_image = video_gen.next()
#         frame_num = frame_num + 1
#     
#     frame_num = 1
#     while video_image is not None and frame_num<250 :
#         print ("Processing video frame" + str(frame_num)) 
#         curr_frame = video_image
#         output = detect_and_classify(model,curr_frame)
#     
#         
#         #u, v = ps4.hierarchical_lk(prev_frame_gray, curr_frame_gray, levels, k_size, k_type, sigma, interpolation,border_mode)
#         #u_v = quiver2(prev_frame, u, v, scale=3, stride=10)
#         
#         #cv2.imshow('output', u_v)
#         
#         frame_id = frame_ids[(output_counter - 1) % 3]
# 
#         if frame_num == 150 or frame_num == 200:
#             out_str = output_name + "-{}.png".format(output_counter)
#             save_image(out_str, curr_frame)
#             image_counter += 1
# 
#         video_final.write(output)
# 
#         video_image = video_gen.next()
#         
#         frame_num += 1
# 
#     video_final.release()
# =============================================================================


def initialize_load_model():

    #test_datagen = ImageDataGenerator(rescale=1./255)

#
    model = load_model('weights.best.custom_model_custom.hdf5')
    model.summary()
    print('model loaded')
    return model
#Create MSER object

def detect_and_classify(model,frame):
    img = frame.copy()
    mser = cv2.MSER_create()
    
    #Your image path i-e receipt path
    #img = cv2.imread('39.png')
    #img = cv2.blur(img,(3,3))
    original_image = img.copy()
    #cv2.imshow('number3',img)
    #cv2.waitKey(0)
    #Convert to gray scale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    vis = img.copy()
    
    #detect regions in gray scale image
    regions, bboxes = mser.detectRegions(gray)
    #print ('bounding boxes =' + str(bboxes))
    hulls = [cv2.convexHull(p.reshape(-1, 1, 2)) for p in regions]
    #print ('hulls')
    #print (hulls)
    
    #print ('length of all hulls = ' + str(len(hulls)))
    #cv2.polylines(vis, hulls, 1, (0, 255, 0))
    
    #cv2.imshow('img', vis)
    
    #cv2.waitKey(0)
    def remove_duplicates(values):
        output = []
        seen = set()
        for value in values:
            # If value has not been encountered yet,
            # ... add it to both list and set.
            if value not in seen:
                output.append(value)
                seen.add(value)
        return output
    
    
    
    #hulls = remove_duplicates(np.asarray(hulls))
    #print ('length of new hulls = ' + str(len(hulls)))
    
    mask = np.zeros((img.shape[0], img.shape[1], 1), dtype=np.uint8)
    
    
    coordinates = []
    for contour in hulls:
        #temp = img
        x,y,w,h = cv2.boundingRect(contour)
        #cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)
        #image_temp = temp[y:y+h, x:x+w]
        #cv2.imshow('image_temp', image_temp)
        #cv2.waitKey(0)
        #images.append(image_temp)
        coordinates.append((x,y,w,h))
        #cv2.drawContours(mask, [contour], -1, (255, 255, 255), -1)
    
    
    #print ('coordinates length = ' +str(len(coordinates)))
    
    coordinates = remove_duplicates(coordinates)
    coordinates = np.asarray(coordinates)
    #print ('new coordinates length = ' +str(len(coordinates)))
    
    mean_coords = np.mean(coordinates,axis=0)
    new_coords = []
    for coord in coordinates:
        if coord[3] > mean_coords[3]:
            if coord[2]/coord[3] < 1.25:
                new_coords.append(coord)
    
    #print ('lenght of final coordinates = ' + str(len(new_coords)))
    
    coordinates = new_coords
    
    #print (new_coords)
    
    def contains(A, B):
        r1x1,r1y1,w,h = A
        r1y2 = r1y1 + h
        r1x2 = r1x1 + w
        
        r2x1,r2y1,w,h = B
        r2y2 = r2y1 + h
        r2x2 = r2x1 + w
        return (r1x1 <= r2x1 and r1y1 <= r2y1 and r2x2 <= r1x2 and r2y2 <= r1y2)
    
    new_coordinates = []
    for i in range(len(new_coords)):
        coords = new_coords[i]
        #print (coords)
        x,y,w,h=coords
        flag = 0
        for j in range(len(new_coords)):
            if (i==j):
                continue
            coords1 = new_coords[j]
            #print (coords1)
            x1,y1,w1,h1=coords1
            #print (contains(coords, coords1))
            if contains(coords1, coords) == True:
                flag = 1
        if flag ==0:
            new_coordinates.append(coords)
                #print (j)
                #new_coordinates.pop(j-remove_count)
                
                
    
    #print (new_coords)
    coordinates = new_coordinates
    
    
    
    images = []
    for coords in coordinates:
        temp = img
        x,y,w,h = coords
        image_temp = temp[y:y+h, x:x+w]
        images.append(image_temp)
    #this is used to find only text regions, remaining are ignored
    #
        
    output_image = original_image
    font = cv2.FONT_HERSHEY_SIMPLEX
    for index, img in enumerate(images):
    #   
        #cv2.imshow('image', img)
        #cv2.waitKey(0)
        img = cv2.resize(img, (32,32),interpolation=cv2.INTER_CUBIC)
    #cv2.imwrite('./data/test_32x32/num3.png',img)
        img = img[np.newaxis,...]
    #test_generator = test_datagen.flow_from_directory(
    #        'data/test_32x32',
    #        target_size=(32, 32),
    #        batch_size=128,
    #        class_mode='categorical',
    #        shuffle=False
    #        )
    #preds = model.predict_generator(test_generator, verbose=1)
    #pred_labels = np.argmax(preds,axis=1)
    #print ('predicted output =' + str(pred_labels))
        x,y,w,h = coordinates[index]
        preds = model.predict(img, verbose=0)
        pred_labels = np.argmax(preds,axis=1)
        #print ('predicted output =' + str(pred_labels))
        if pred_labels < 10:
            cv2.rectangle(output_image,(x,y),(x+w,y+h),(0,255,0),2)
            cv2.putText(output_image,str(pred_labels[0]),(x,y), font, 1,(0,255,0),2,cv2.LINE_AA)
    
    #cv2.imwrite('door1_output.png',output_image)
    #cv2.imshow('output image', output_image)   
    #cv2.waitKey(0)
    return output_image
#pred_labels = np.argmax(preds,axis=1)

#text_only = cv2.bitwise_and(img, img, mask=mask)

#cv2.imshow("text only", text_only)

#cv2.waitKey(0)

if __name__ == "__main__":
    #load_images_and_run(folder)
    video()