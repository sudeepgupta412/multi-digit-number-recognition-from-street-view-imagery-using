# Classification and Detection with Convolutional Neural Networks

## Introduction
Detecting and reading text from natural images is a hard computer vision task that is central to a variety of emerging applications. Related problems like document character recognition have been widely studied by computer vision and machine learning researchers. Reading the characters from a natural scene is far more difficult problem. There has been research in this field for a long time using methods like Deep Convolutional Neural Networks, Unsupervised feature learning and many more.
This project consisted of two parts. The first is to detect the digits in a given scene and the second is to classify the digits between (0-9). To detect the digits, we used Maximally Stable Extremal External Regions(MSER), a feature detector. We used our own custom CNN model to classify them. For classification the first step was to train the model with some data. We used an Untrained VGG-16 model, a custom CNN model and a trained VGG-16 model. We used Street View House Numbers (SVHN) Dataset with over 600,000 digits images to train our models.

## Pipeline
When we send an image to the program, the first step the image goes through is to identify the digits in the image using MSER. Once we have the raw detected digit regions, we process those regions to remove the redundant areas of identification. After this, we extract the portion of the image corresponding to the refined detected regions and pass these extracted image portions through our classifier model (in this case our own CNN custom model). The model identifies the respective digits and highlight the digit portion in the image along with the predicted output.

## Instructions to Run the code ##

	- "data-Preprocessing.ipynb" - contains code to preprocess the data and classify them in seperate directories
	
	- "data-Training-Testing.ipynb" - contains code to train and test data using untrained VGG-16 Model, pretrained VGG-16 Model and a custom model.
	
	- put all the weights in the location where run_v3.py file is present
	
	- create an "output" directory
	
	- run "run_v3.py"
	
## Results	

![output1](output/door1_output.png)

![output2](output/door4_output.png)

![output3](output/27_output.png)

![output4](output/26_output.png)

![output5](output/28_output.png)
